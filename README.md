## Drafts

- [Migration Guide from Git Annex to Git LFS](https://gitlab.com/marcia/drafts/blob/master/migration-guide-from-git-annex-to-git-lfs.md) => https://gitlab.com/gitlab-org/gitlab-ee/merge_requests/1230
- GitLab Pages Docs
  - [Index](https://gitlab.com/marcia/drafts/blob/master/pages/index.md) (Pages resources - Docs, Guides, Posts)
      - GitLab Pages from A to Z [closes https://gitlab.com/gitlab-org/gitlab-ce/issues/28097]
          - [Part 1: Static Sites, Domains, DNS Records, and SSL/TLS Certificates](https://gitlab.com/marcia/drafts/blob/master/pages/pages_part-1.md) [closes https://gitlab.com/gitlab-org/gitlab-ee/issues/994 and https://gitlab.com/gitlab-org/gitlab-ce/issues/28098]
          - [Part 2: Quick Start Guide - Setting Up GitLab Pages](https://gitlab.com/marcia/drafts/blob/master/pages/pages_part-2.md)
          - [Part 3: Creating and Tweaking `.gitlab-ci.yml` for GitLab Pages](https://gitlab.com/marcia/drafts/blob/master/pages/pages_part-3.md)

## Quick update

Hello world