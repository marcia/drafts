- Draft for refactoring https://docs.gitlab.com/ee/user/project/pages/

-----

### Getting Started

With GitLab Pages you can have a website live at no cost.

_Minimum requirements to have your website live:_

1. An account on GitLab
2. A project
3. Enabled GitLab Runners to deploy your website with GitLab CI/CD

_Minimum steps:_

- 1. Fork an example project
- 2. Enable Shared Runners
- 3. Change a file (trigger the pipeline)
- 4. Visit Pages settings to see your website link, and click on it. Bam! Your website is live.

_Further steps (optional):_

- 5. Remove the fork relationship
- 6. Make it a user/group website 

**Watch the video with the steps 1-6: https://www.youtube.com/watch?v=TWqh9MtT4Bg**

_Advanced options:_

- Use a custom domain
- Apply SSL/TLS to your custom domain

### Explore GitLab Pages

- About static websites and Pages domains: https://docs.gitlab.com/ce/user/project/pages/getting_started_part_one.html
- About forking projects and creating new ones from scratch, URLs and baseurls: https://docs.gitlab.com/ce/user/project/pages/getting_started_part_two.html
- About custom domains and subdomains, DNS records, SSL/TLS certificates: https://docs.gitlab.com/ce/user/project/pages/getting_started_part_three.html 
- How to create your own `.gitlab-ci.yml` for your site: https://docs.gitlab.com/ce/user/project/pages/getting_started_part_four.html
- Technical aspects, custom 404 pages, limitations: https://docs.gitlab.com/ee/user/project/pages/introduction.html
- Hosting on GitLab.com with GitLab Pages (outdated) - https://about.gitlab.com/2016/04/07/gitlab-pages-setup/

_Blog posts series about Static Site Generators (SSGs):_

- Static vs dynamic websites
- Modern static site generators
- Build any SSG site with GitLab Pages

_Blog posts for securing GitLab Pages custom domains with SSL/TLS certificates:_

- CloudFlare
- Let's Encrypt (outdated)
- StartSSL (deprecated)

### Advanced use

- Posting to your GitLab Pages blog from iOS
- GitLab CI: Run jobs sequentially, in parallel, or build a custom pipeline
- GitLab CI: Deployment & environments
- Building a new GitLab docs site with Nanoc, GitLab CI, and GitLab Pages
- Publish code coverage reports with GitLab Pages

### GitLab Pages for CE and EE

Enable and configure GitLab Pages on your own instance (GitLab Community Editions and Enterprise Editions) with
the [Admin guide](https://docs.gitlab.com/ee/administration/pages/index.html).

**Watch the video: https://www.youtube.com/watch?v=dD8c7WNcc6s**


-------

NOTES (NOT PART OF THE DOC):

Why do we need:

- (2) Enable shared runners:

![initial runner settings](runner_settings.png)

- (2) See what happens if we don't enable shared runners:

![pipeline pending forever](pipeline_pending.png)

- (2/3) Once you enable shared runners:

![pipeline running](pipeline_running.png)


