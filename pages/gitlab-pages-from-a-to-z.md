
# UPDATE: See this doc split in 3: [Part 1, Part 2, Part 3](index.md)

## UPDATE 2: I'll make the part 2 the quick start guide.

----

# GitLab Pages from A to Z

> Type: user guide
>
> Level: beginner

On this tutorial we'll walk you through the entire process of hosting a website with [GitLab Pages]. This is a comprehensive guide, made for those who want to publish a website with GitLab Pages but aren't familiar with the entire process involved.

To **enable** GitLab Pages for GitLab CE and GitLab EE, please read the [admin documentation][admin-doc], or watch this [video tutorial][video-pages].

> We assume you already have GitLab Pages server up and running for your GitLab instance.

## What you need to know before getting started

Before we begin, let's understand a few concepts first.

### Static Sites

GitLab Pages only supports static websites, meaning, your output files must be HTML, CSS, and JavaScript only.

To create your static site, you can either hardcode in HTML, CSS, and JS, or use a [Static Site Generator (SSG)][ssg] to simplify your code and build the static site for you, which is highly recommendable and much faster than hardcoding.

#### Further Reading

- Read through this technical overview on [Static versus Dynamic Websites][post-ssg1]
- Understand [how modern Static Site Generators work][post-ssg2] and what you can add to your static site
- You can use [any SSG with GitLab Pages][post-ssg3]
- Fork an [example project][pages-examples] to build your website based upon

### GitLab Pages Domain

If you set up a GitLab Pages project on GitLab.com, it will automatically be accessible under a [subdomain of `<namespace>.pages.io`][pages-user-docs]. The `<namespace>` is defined by your username on GitLab.com, or the group name you created this project under.

> Note: If you use your own GitLab instance to deploy your site with GitLab Pages, check with your sysadmin what's your Pages wildcard domain. This guide is valid for any GitLab instance, you just need to replace Pages wildcard domain on GitLab.com (`*.gitlab.io`) with your own.

#### Practical examples

Project Websites:

- You created a project called `blog` under your username `john`, therefore your project URL is `https://gitlab.com/john/blog/`. Once you enable GitLab Pages for this project, and build your site, it will be available under `https://john.gitlab.io/blog/`.
- You created a group for all your websites called `websites`, and a project within this group is called `blog`. Your project URL is `https://gitlab.com/websites/blog/`. Once you enable GitLab Pages for this project, the site will live under `https://websites.gitlab.io/blog/`.

User and Group Websites:

- Under your username, `john`, you created a project called `john.gitlab.io`. Your project URL will be `https://gitlab.com/john/john.gitlab.io`. Once you enable GitLab Pages for your project, your website will be published under `https://john.gitlab.io`.
- Under your group `websites`, you created a project called `websites.gitlab.io`. your project's URL will be `https://gitlab.com/websites/websites.gitlab.io`. Once you enable GitLab Pages for your project, your website will be published under `https://websites.gitlab.io`.

General example:

- On GitLab.com, a project site will always be available under `https://namespace.gitlab.io/project-name`
- On GitLab.com, a user or group website will be available under `https://namespace.gitlab.io/`
- On your GitLab instance, replace `gitlab.io` above with your Pages server domain. Ask your sysadmin for this information.

### URLs and Baseurls

You happily deployed you project website, then you access it and see that your CSS styles aren't being applied to the website at all. This is quite common to happen to project websites (not to user or group websites). Search the documentation of the SSG you're using to build your site looking for baseurls. For our Jekyll site, the `baseurl` is defined in the Jekyll configuration file, `_config.yml`. If your website URL is `https://john.gitlab.io/blog/`, you need to add this line to `_config.yml`:

```yaml
baseurl: "/blog"
```


### DNS Records

A Domain Name System (DNS) web service routes visitors to websites by translating domain names (such as www.example.com) into the numeric IP addresses (such as `192.0.2.1`) that computers use to connect to each other.

A DNS record is created to point a (sub)domain to a certain location, which can be an IP address or another domain. In case you want to use GitLab Pages with your own (sub)domain, you need to access your domain's registrar control panel to add a DNS record pointing it back to your GitLab Pages site.

Note that **how to** add DNS records depends on which server your domain is hosted on. Every control panel has its own place to do it. If you are not an admin of your domain, and don't have access to your registrar, you'll need to ask for the technical support of your hosting service to do it for you.

To help you out, we've gathered some instructions on how to do that for the most popular hosting services:

- [Amazon](http://docs.aws.amazon.com/gettingstarted/latest/swh/getting-started-configure-route53.html)
- [Bluehost](https://my.bluehost.com/cgi/help/559)
- [CloudFlare](https://support.cloudflare.com/hc/en-us/articles/200169096-How-do-I-add-A-records-)
- [cPanel](https://documentation.cpanel.net/display/ALD/Edit+DNS+Zone)
- [DreamHost](https://help.dreamhost.com/hc/en-us/articles/215414867-How-do-I-add-custom-DNS-records-)
- [Go Daddy](https://www.godaddy.com/help/add-an-a-record-19238)
- [Hostgator](http://support.hostgator.com/articles/changing-dns-records)
- [Inmotion hosting](https://my.bluehost.com/cgi/help/559)
- [Media Temple](https://mediatemple.net/community/products/dv/204403794/how-can-i-change-the-dns-records-for-my-domain)
- [Microsoft](https://msdn.microsoft.com/en-us/library/bb727018.aspx)

If your hosting service is not listed above, you can just try to search the web for "how to add dns record on <my hosting service>".

#### DNS A record

In case you want to point a root domain (`example.com`) to your GitLab Pages site, deployed to `namespace.gitlab.io`, you need to log into your domain's admin control panel and add a DNS `A` record pointing your domain to Pages' server IP address. For projects on GitLab.com, this IP is `104.208.235.32`. For projects leaving in other GitLab instances (CE or EE), please contact your sysadmin asking for this information (which IP address is Pages server running on your instance).

Practical Example:

![DNS A record pointing to GitLab.com Pages server]()

#### DNS CNAME record

In case you want to point a subdomain (`hello-world.example.com`) to your GitLab Pages site initially deployed to `namespace.gitlab.io`, you need to log into your domain's admin control panel and add a DNS `CNAME` record pointing your subdomain to your website URL (`namespace.gitlab.io`) address.

Notice that, despite it's a user or project website, the `CNAME` should point to your Pages domain (`namespace.gitlab.io`), without any `/project-name`.

Practical Example:

![DNS CNAME record pointing to GitLab.com project]()

#### TL;DR

| From | DNS Record | To |
| ---- | ---------- | -- |
| domain.com | A | 104.208.235.32 |
| subdomain.domain.com | CNAME | namespace.gitlab.io |

> **Notes**:
>
> - **Do not** use a CNAME record if you want to point your `domain.com` to your GitLab Pages site. Use an `A` record instead.
> - **Do not** add any special chars after the default Pages domain. E.g., **do not** point your `subdomain.domain.com` to `namespace.gitlab.io.` or `namespace.gitlab.io/`.

### SSL/TLS Certificates

Every GitLab Pages project on GitLab.com will be available under HTTPS for the default Pages domain (`*.gitlab.io`). Once you set up your Pages project with your custom (sub)domain, if you want it secured by HTTPS, you will have to issue a certificate for that (sub)domain and install it on your project.

> Note: certificates are NOT required to add to your custom (sub)domain on your GitLab Pages project, though they are highly recommendable.

The importance of having any website securely served under HTTPS is explained on the introductory section of the blog post [Secure GitLab Pages with StartSSL][post-startssl].

The reason why certificates are so important is that they encrypt the connection between the **client** (you, me, your visitors) and the **server** (where you site lives), through a keychain of authentications and validations.

### Issuing Certificates

GitLab Pages accepts [PEM] certificates issued by Certificate Authorities (CA) and self-signed certificates. Of course, [you'd rather issue a certificate than generate a self-signed][self-signed], for security reasons and for having browsers trusting your site's certificate.

There are several different kinds of certificates, each one with certain security level. A static personal website will not require the same security level as an online banking web app, for instance. There are a couple Certificate Authorities that offer free certificates, aiming to make the internet more secure to everyone. The most popular is [Let's Encrypt][lets-encrypt], which issues certificates trusted by most of browsers, it's open source, and free to use. Please read through this tutorial to understand [how to secure your GitLab Pages website with Let's Encrypt][post-lets].

With the same popularity, there are [certificates issued by CloudFlare][CloudFlare], which also offers a [free CDN service][cloud-cdn]. Their certs are valid up to 15 years. Read through the tutorial on [how to add a CloudFlare Certificate to your GitLab Pages website][post-cloud].

#### Further reading

- Understand [PEM certificate formats](https://support.quovadisglobal.com/kb/a37/what-is-pem-format.aspx)
- [Certificate Authorities](https://en.wikipedia.org/wiki/Certificate_authority)

### Adding certificates to your project

Doesn't matter which CA you chose, the steps to add your cert to your Pages project are the same.

#### What do you need

1. A PEM certificate
1. An intermediary certificate
1. A public key

![Pages project - adding certificates]()

These fields are found under your **Project**'s **Settings** > **Pages** > **New Domain**.

#### What's what?

- A PEM certificate is the certificate generated by the CA, which needs to be added to the field **Certificate (PEM)**.
- An [intermediary certificate][] (aka "root certificate") is the part of the encryption keychain that identifies the CA. Usually it's combined with the PEM certificate, but there are some cases in which you need to add them manually. [CloudFlare certs][post-cloud] are one of these cases.
- A public key is an encrypted key which validates your PEM against your domain.

#### Now what?

Now that you hopefully understand why you need all of this, it's simple:

- Your PEM certificate needs to be added to the first field
- If your certificate is missing its intermediary, copy and paste the root certificate (usually available from your CA website) and paste it in the [same field as your PEM certificate][post-cloud], just jumping a line between them.
- Copy your public key and paste it in the last field

> Note: **do not** open certificates or encryption keys in regular text editors. Always use code editors (such as Sublime Text, Atom, Dreamweaver, Brackets).

## Setting Up GitLab Pages

For a step-by-step tutorial, please read the blog post [Hosting on GitLab.com with GitLab Pages][post-pages-on-dotcom]. The following sections will explain what do you need and why do you need them.

<!-- todo: transfer the content from that post to docs -->

### What You Need to Get Started

1. A project
1. A configuration file (`.gitlab-ci.yml`) to deploy your site
1. A specific `job` called `pages` in the configuration file that will make GitLab aware that you are deploying a GitLab Pages website

#### Optional Features

1. A custom domain or subdomain
1. A DNS pointing your (sub)domain to your Pages site
   1. **Optional**: an SSL/TLS certificate so your custom domain is accessible under HTTPS.

### Project

Your GitLab Pages project is a regular project created the same way you do for the other ones. What you can do is, instead of creating a new project from scratch, fork one of the templates from Page Examples. Let's go over both options.

#### Create a Project from Scratch

1. From your **Project**'s **[Dashboard]**, click **New project**, and name it considering the [examples above](#practical-examples).
1. Clone it to your local computer, add your website files, add, commit and push to GitLab.
1. From the your **Project**'s page, click the plus sign and choose **Add new file**:
    
    ![add new file]()

1. Type `.gitlab-ci.yml`in the file name field , then click on the text editor, so you'll see a dropbox from where you can choose one of the templates. Pick up the template corresponding to the SSG you're using (or plain HTML).

    ![gitlab-ci templates]()

> Note: GitLab Pages [supports any SSG][post-ssg3], but, if you don't find yours among the templates, you'll need to configure your own `.gitlab-ci.yml`. Do do that, please read through the [section below](#creating-and-tweaking-gitlab-ciyml-for-gitlab-pages). New SSGs are very welcome among the [example projects][pages-examples]. If you set up a new one, please [contribute][pages-contribute] to our examples.

Once you have both site files and `.gitlab-ci.yml` in your project's root, GitLab CI will build your site and deploy it with Pages. Once the first build passes, you see your site is live by navigating to your **Project**'s **Settings** > **Pages**, where you'll find its default URL.

#### Fork a Project to Get Started From

To make things easy for you, we've created this [group][pages-examples] of default projects containing some SSGs templates.

1. Choose your SSG template
1. Fork the project
1. Remove the fork relationship
  
    ![remove fork relashionship]()

1. Trigger a build (push a change to any file)
1. As soon as the build passes, your website will have been deployed with GitLab Pages. Your website URL will be available under your **Project**'s **Settings** > **Pages**

> Note: Why do I need to remove the fork relationship?
>
> Unless you want to contribute to the original project, you won't need it connected to the upstream. A [fork][post-fork] is useful for submitting merge requests to the upstream.

### Creating and Tweaking `.gitlab-ci.yml` for GitLab Pages

[GitLab CI][ci] serves inumerous purposes, to build, test, and deploy your app from GitLab through [Continuous Integration, Continuous Delivery, and Continuous Deployment][post-ci-cd] methods. You will need it to build your website with GitLab Pages, and deploy it to the Pages server.

What this file actually does is telling the [GitLab Runner][runner] to run scripts as you would do from the command line. The Runner acts as your terminal. GitLab CI tells the Runner which commands to run. Both are built-in in GitLab, and you don't need to set up anything for them to work.

Explaining [every detail of GitLab CI][ci-docs] and GitLab Runner is out of the scope of this guide, but we'll need to understand just a few things to be able to write our own `.gitlab-ci.yml` or tweak an existing one. It's an [Yaml] file, with its own syntax. You can always check your CI syntax with the [GitLab CI Lint Tool][lint].

Practical Example:

Let's consider you have a Jekyll site. To build it locally, you would open your terminal, and run `jekyll build`. Of course, before building it, you had to install Jekyll in your computer. For that, you had to open your terminal and run `gem install jekyll`. Right? GitLab CI + GitLab Runner do the same thing. But you need to write in the `.gitlab-ci.yml` the script you want to run so the GitLab Runner will do it for you. It looks more complicated then it is. What you need to tell the Runner:

```
$ gem install jekyll
$ jekyll build
```

#### Script

To transpose this script to Yaml, it would be like this:

```yaml
script:
  - gem install jekyll
  - jekyll build
```

#### Job

So far so good. Now, each `script`, in GitLab is organized by a `job`, which is a bunch of scripts and settings you want to apply to that specific task.

```yaml
job:
  script:
  - gem install jekyll
  - jekyll build
```

For GitLab Pages, this `job` has a specific name, called `pages`, which tells the Runner you want that task to deploy your website with GitLab Pages:

```yaml
pages:
  script:
  - gem install jekyll
  - jekyll build
```

#### `public` Dir

We also need to tell Jekyll where do you want the website to build, and GitLab Pages will only consider files in a directory called `public`. To do that with Jekyll, we need to add a flag specifying the [destination (`-d`)][jekyll-d] of the built website: `jekyll build -d public`. Of course, we need to tell this to our Runner:

```yaml
pages:
  script:
  - gem install jekyll
  - jekyll build -d public
```

#### Artifacts

We also need to tell the Runner that this _job_ generates _artifacts_, which is the site built by Jekyll. Where are these artifacts stored? In the `public` directory:

```yaml
pages:
  script:
  - gem install jekyll
  - jekyll build -d public
  artifacts:
    paths:
    - public
```

The script above would be enough to build your Jekyll site with GitLab Pages. But, from Jekyll 3.4.0 on, its default template originated by `jekyll new project` requires [Bundler] to install Jekyll dependencies and the default theme. To adjust our script to meet these new requirements, we only need to install and build Jekyll with Bundler:

```yaml
pages:
  script:
  - bundle install
  - bundle exec jekyll build -d public
  artifacts:
    paths:
    - public
```

That's it! A `.gitlab-ci.yml` with the content above would deploy your Jekyll 3.4.0 site with GitLab Pages.

#### Image

At this point, you probably ask yourself: "okay, but to install Jekyll I need Ruby. Where is Ruby on that script?". The answer is simple: the first thing GitLab Runner will look for in your `.gitlab-ci.yml` is a [Docker] image specifying what do you need in your container to run that script:

```yaml
image: ruby:2.3

pages:
  script:
  - bundle install
  - bundle exec jekyll build -d public
  artifacts:
    paths:
    - public
```

In this case, you're telling the Runner to pull this image, which contains Ruby 2.3 as part of its file system. When you don't specify this image in your configuration, the Runner will use a default image, which is Ruby 2.1.

If your SSG needs [NodeJS] to build, you'll need to specify which image you want to use, and this image should contain NodeJS as part of its file system. E.g., for a [Hexo] site, you can use `image: node:4.2.2`.

> Note: we're not trying to explain what a Docker image is, we just need to introduce the concept with a minimum viable explanation. To know more about Docker images, please visit their website or take a look at a [summarized explanation][docker-sto] here.

Let's go a little further.

#### Branching

If you use GitLab as a version control platform, you will have your branching strategy to work on your project. Meaning, you will have other branches in your project, but you'll want only pushes to the default branch (usually `master`) to be deployed to your website. To do that, we need to add another line to our CI, telling the Runner to only perform that _job_ called `pages` on the `master` branch `only`:

```yaml
image: ruby:2.3

pages:
  script:
  - bundle install
  - bundle exec jekyll build -d public
  artifacts:
    paths:
    - public
  only:
  - master
```

#### Stages

Another interesting concept to keep in mind are build stages. Your web app can pass through a lot of tests and other tasks until it's deployed to staging or production environments. There are three default stages on GitLab CI: build, test, and deploy. To specify which stage your _job_ is running, simply add another line to your CI:

```yaml
image: ruby:2.3

pages:
  stage: deploy
  script:
  - bundle install
  - bundle exec jekyll build -d public
  artifacts:
    paths:
    - public
  only:
  - master
```

You might ask yourself: "why should I bother with stages at all?" Well, let's say you want to be able to test your script and check the built site before deploying your site to production. You want to run the test exactly as your script will do when you push to `master`. It's simple, let's add another task (_job_) to our CI, telling it to test every push to other branches, `except` the `master` branch:

```yaml
image: ruby:2.3

pages:
  stage: deploy
  script:
  - bundle install
  - bundle exec jekyll build -d public
  artifacts:
    paths:
    - public
  only:
  - master

test:
  stage: test
  script:
  - bundle install
  - bundle exec jekyll build -d test
  artifacts:
    paths:
    - test
  except:
  - master
```

The `test` job is running on the stage `test`, Jekyll will build the site in a directory called `test`, and this job will affect all the branches except `master`.

The best benefit of applying _stages_ to different _jobs_ is that every job in the same stage builds in parallel. So, if your web app needs more than one test before being deployed, you can run all your test at the same time, it's not necessary to wait on test to finish to run the other. Of course, this is just a brief introduction of GitLab CI and GitLab Runner, which are tools much more powerful than that. This is what you need to be able to create and tweak your builds for your GitLab Pages site.

#### Before Script

To avoid running the same script multiple times across your _jobs_, you can add the parameter `before_script`, in which you specify which commands you want to run for every single _job_. In our example, notice that we run `bundle install` for both jobs, `pages` and `test`. We don't need to repeat it:

```yaml
image: ruby:2.3

before_script:
  - bundle install

pages:
  stage: deploy
  script:
  - bundle exec jekyll build -d public
  artifacts:
    paths:
    - public
  only:
  - master

test:
  stage: test
  script:
  - bundle exec jekyll build -d test
  artifacts:
    paths:
    - test
  except:
  - master
```

#### Caching Dependencies

If you want to cache the installation files for your projects dependencies, for building faster, you can use the parameter `cache`. For this example, we'll cache Jekyll dependencies in a `vendor` directory when we run `bundle install`:

```yaml
image: ruby:2.3

cache:
  paths:
  - vendor/

before_script:
  - bundle install --path vendor

pages:
  stage: deploy
  script:
  - bundle exec jekyll build -d public
  artifacts:
    paths:
    - public
  only:
  - master

test:
  stage: test
  script:
  - bundle exec jekyll build -d test
  artifacts:
    paths:
    - test
  except:
  - master
```

For this specific case, we need to exclude `/vendor` from Jekyll `_config.yml` file, otherwise Jekyll will understand it as a regular directory to build together with the site:

```yml
exclude:
  - vendor
```

There we go! Now our GitLab CI not only builds our website, but also **continuously test** pushes to feature-branches, **caches** dependencies installed with Bundler, and **continuously deploy** every push to the `master` branch.

### Advanced GitLab CI for GitLab Pages

What you can do with GitLab CI is pretty much up to your creativity. Once you get used to it, you start creating awesome scripts that automate most of tasks you'd do manually in the past. Read through the [documentation of GitLab CI][ci-docs] to understand how to go even further on your scripts.

- In this blog post, understand the concept of [using GitLab CI `environments` to deploy your web app to staging and production][post-ivan].
- In this blog post, we go through the process of [pulling specific directories from different projects][post-connor] to deploy this website you're looking at, docs.gitlab.com.
- In this blog post, we teach you [how to use GitLab Pages to produce a code coverage report][post-grzegorz].



### Redirects

Read throgh [GitLab Pages main docs][pages-docs-redirects].



<!-- identifiers -->

<!-- docs -->

[pages-user-docs]: https://docs.gitlab.com/ce/user/project/pages/index.html
[pages-docs-redirects]: https://docs.gitlab.com/ce/user/project/pages/index.html#redirects-in-gitlab-pages
[admin-doc]: https://docs.gitlab.com/ce/administration/pages/index.html
[ci-docs]: https://docs.gitlab.com/ce/ci/yaml/README.html
[runner]: https://docs.gitlab.com/runner/

<!-- general -->

[Bundler]: http://bundler.io/
[ci]: https://about.gitlab.com/gitlab-ci/
[CloudFlare]: https://www.cloudflare.com/ssl/
[dashboard]: https://gitlab.com/dashboard/projects
[docker-sto]: http://paislee.io/how-to-automate-docker-deployments/
[docker]: https://www.docker.com/
[GitLab Pages]: https://pages.gitlab.io
[Hexo]: https://gitlab.com/pages/hexo
[jekyll-d]: https://jekyllrb.com/docs/usage/
[jekyll]: https://jekyllrb.com
[lets-encrypt]: https://letsencrypt.org/
[lint]: https://gitlab.com/ci/lint
[NodeJS]: https://nodejs.org
[pages-contribute]: https://gitlab.com/pages/pages.gitlab.io/blob/master/CONTRIBUTING.md
[pages-examples]: https://gitlab.com/groups/pages
[PEM]: https://en.wikipedia.org/wiki/Privacy-enhanced_Electronic_Mail
[post-ci-cd]: https://about.gitlab.com/2016/08/05/continuous-integration-delivery-and-deployment-with-gitlab/
[post-cloud]: https://about.gitlab.com/2017/02/07/setting-up-gitlab-pages-with-cloudflare-certificates/
[post-connor]: https://about.gitlab.com/2016/12/07/building-a-new-gitlab-docs-site-with-nanoc-gitlab-ci-and-gitlab-pages/
[post-fork]: https://about.gitlab.com/2016/12/01/how-to-keep-your-fork-up-to-date-with-its-origin/#fork
[post-grzegorz]: https://about.gitlab.com/2016/11/03/publish-code-coverage-report-with-gitlab-pages/
[post-ivan]: https://about.gitlab.com/2016/08/26/ci-deployment-and-environments/
[post-lets]: https://about.gitlab.com/2016/04/11/tutorial-securing-your-gitlab-pages-with-tls-and-letsencrypt/
[post-pages-on-dotcom]: https://about.gitlab.com/2016/04/07/gitlab-pages-setup/
[post-ssg1]: https://about.gitlab.com/2016/06/03/ssg-overview-gitlab-pages-part-1-dynamic-x-static/
[post-ssg2]: https://about.gitlab.com/2016/06/10/ssg-overview-gitlab-pages-part-2/
[post-ssg3]: https://about.gitlab.com/2016/06/17/ssg-overview-gitlab-pages-part-3-examples-ci/
[post-startssl]: https://about.gitlab.com/2016/06/24/secure-gitlab-pages-with-startssl/#https-a-quick-overview
[root]: https://en.wikipedia.org/wiki/Root_certificate
[sef-signed]: https://www.sslshopper.com/article-how-to-create-a-self-signed-certificate.html
[video-pages]: #
[yaml]: http://docs.ansible.com/ansible/YAMLSyntax.html
[ssg]: https://www.staticgen.com/

